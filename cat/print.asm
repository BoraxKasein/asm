section .data
    newline db 10

section .text

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Print a char. Argument is a pointer to a character.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
print_char:
	mov rax, 1              ; syscall to sys_write
	mov rdi, 1              ; stdout
    mov rsi, [rsp+8]        ; Oben auf stack ist ret addr, die is 8 byte lang, danach kommt arg 1
	mov rdx, 1              ; buffer len
	syscall                 ; führe syscall aus
    ret                     ; schmeiß arg1 weg


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Print a string. Arguent is a pointer to a null terminated string
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
print_str:
    mov rcx, [rsp+8]        ; rcx = pointer zu string (erstes argument)

    mov rax, 1              ; syscall to sys_write
	mov rdi, 1              ; stdout
    mov rsi, [rsp+8]        ; buffer pointer sys_write
    mov rdx, 0              ; buffer len for sys_write

.not_null:
    movsx r8, byte [rcx]
    inc rcx 
    inc rdx
    cmp r8b, byte 0
    jne .not_null

    dec rdx
	syscall                 ; führe syscall aus
    ret

    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Print a string with newline on the end. Arguent is a pointer to a null terminated string
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
print_str_ln:
    push qword [rsp+8]
    call print_str
    pop rax

    push newline
    call print_char
    pop rax
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Return the next argument of argv. All strings must be terminated with null and must be in
;; one continouse memory area
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
space_tok:
    mov rax, [rsp+8]        ; rax = pointer zu string (erstes argument)
.not_null:
    movsx rcx, byte [rax]
    inc rax 
    cmp cl, byte 0
    jne .not_null
    ret
