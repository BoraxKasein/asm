; General tutorial          https://0xax.github.io/asm_1/
; Command line arguments:   https://github.com/tonyOreglia/argument-counter/wiki/x86-64-Linux-Assembly-Part-1:-Printing-Command-Line-Arguments
; Registers:                https://wiki.cdot.senecacollege.ca/wiki/X86_64_Register_and_Instruction_Quick_Start
; System functions:         https://elixir.bootlin.com/linux/latest/source/include/linux/syscalls.h#L463
; System call numbers:      https://de.wikipedia.org/wiki/Liste_der_Linux-Systemaufrufe
; x86_64 instruction set:   https://www.felixcloutier.com/x86/
; call numbers & functions: https://filippo.io/linux-syscall-table/
%include "print.asm"

%define BUF_LEN 64

; .data section is for constants
section .data
    error       db   "Error  ",0x0A     ; Error string     
    error_len   equ  $-error            ; Error string length

    buf times BUF_LEN db 0

; .text section is for code
; must start with global _start
; kernel starts the program at _start
section .text
	global _start

; rax enthält syscall nummer.
; rdi ist für das erste argument
; rsi ist für das zweite argument
; rdx ist für das dritte argument
; usw.:
; rdi, rsi, rdx, rcx, r8, r9
; wenn mehr als 6 argumente, weitere werden auf stack gelegt,
; siehe "The 64 bit x86 C Calling Convention"

; size_t sys_write(unsigned int fd, const char * buf, size_t count);
; long sys_open(const char __user *filename, int flags, umode_t mode);
; long sys_read(unsigned int fd, char __user *buf, size_t count);

_start:

    ; read command line arguments
    pop rdi                 ; argc
    cmp rdi, 2              ; check if one argument is given
    mov [error + error_len - 2], byte 49
    jne err_hdl             ; jump if not equal
    pop rdi                 ; argv

    ; print first arg
    push rdi
    call print_str_ln

    ; get second arg
    call space_tok
    pop r8

    ;print second arg
    push rax 
    call print_str_ln
    pop rax

    ; open file in rdi
    mov rdi, rax            ; second arg is file to cat
    mov rax, 2              ; syscall open
    mov rsi, 0              ; O_RDONLY   
    mov rdx, 0              ; mode_t
    syscall
    cmp rax, 0              ; check if open throws an error (-1)
    mov [error + error_len - 2], byte 50 
    jl  err_hdl             ; jump if lower, signed compare
    mov r10, rax            ; save fd in r10

cat:
    ; loop
    ; read from file
    mov rax, 0              ; syscall read
    mov rdi, r10            ; file
    mov rsi, buf            ; read into char defined in .data
    mov rdx, BUF_LEN        ; read one byte
    syscall
    cmp rax, 0              ; check if eof is reached
    jle exit                ; jump if rax == 0

    ; wirte to stdout
    
    mov rdx, rax            ; len from sys_read
    mov rax, 1              ; syscall to sys_write
    mov rdi, 1              ; write to stdout
    mov rsi, buf            ; buffer to write to stdout
    syscall
    cmp rax, 0              ; check if write was successfull
    mov [error + error_len - 2], byte 51
    jl err_hdl              ; jump if lower, signed comapre
    jmp cat                 ; jump too loop begin (cat)

err_hdl:
    ; print error to stdout
	mov rax, 1              ; syscall to sys_write
	mov rdi, 1              ; stdout
	mov rsi, error          ; buffer
	mov rdx, error_len      ; buffer len
	syscall                 ; führe syscall aus

exit:
    ; close file
    mov rax, 3              ; syscall to close
    mov rdi, r10            ; close previously opened fd
    syscall    
exit_raw:
    ; call exit
	mov rax, 60             ; syscall to exit
	mov rdi, 0              ; erster parameter 0
	syscall                 ; call exit
