%include "std.asm"

%define BUFFER_LEN 32
section .data
    putc_msg db "Test std_put_c(_ln). Output: x x",0
    puti_msg db "Test std_put_int. Output: 661 34566543",0
    stack_msg db "Test Stack Pointer. Output: 0 8 16 18 0",0
    getc_msg db "Test std_get_c, press a key: ",0
    getl_msg db "Test std_get_line, type something: ",0

    malloc_msg db "Test std_malloc, try to allocate 256 bytes: ",0
    mal_w_msg db "Write some byte in allocated area: ",0
    mal_point db "Address is: ",0
    mal_free db "Freeing: ",0

    fail_msg db "Failed",0
    succ_msg db "Success",0

    stack_base dq 0
    buffer times BUFFER_LEN db 0

section .text
	global _start
_start:
    ;;;;;; Test std_put_str and std_put_str_ln ;;;;;
    mov rdi, putc_msg    
    call std_put_str_ln

    ;;;;;; Test std_put_c and std_put_c_ln ;;;;;
    mov rdi, 'x'
    call std_put_c

    mov rdi, ' '
    call std_put_c

    mov rdi, 'x'
    call std_put_c_ln

    ;;;;;; Test std_put_int ;;;;;
    mov rdi, puti_msg
    call std_put_str_ln
    add rsp, 8

    mov rdi, 661
    call std_put_int_ln

    mov rdi, 34566543
    call std_put_int_ln
    
    ;;;;;; Test stack ;;;;;
    mov rdi, stack_msg
    call std_put_str_ln
    mov [stack_base], rsp

    ; 0
    mov rdi, [stack_base]
    sub rdi, rsp 
    call std_put_int_ln

    ; 8
    push rax                ; push 8 byte
    mov rdi, [stack_base]
    sub rdi, rsp
    call std_put_int_ln

    ; 16 
    push rax                ; push 8 byte
    mov rdi, [stack_base]
    sub rdi, rsp
    call std_put_int_ln

    ; 18
    push ax                 ; push 2 byte
    mov rdi, [stack_base]
    sub rdi, rsp
    call std_put_int_ln

    ; Clean stack
    pop ax                  ; pop everything
    pop rax
    pop rax
    mov rdi, [stack_base]
    sub rdi, rsp
    call std_put_int_ln

    ;;;;;; Test std_get_char ;;;;;
    mov rdi, getc_msg 
    call std_put_str

    call std_get_c
    mov rdi, rax
    call std_put_c_ln

    ;;;;;; Test std_get_line ;;;;;
    mov rdi, getl_msg
    call std_put_str

    mov rdi, buffer
    mov rsi, BUFFER_LEN
    call std_get_line

    mov rdi, buffer
    call std_put_str_ln

    ;;;;; Test std_malloc ;;;;;
    call test_malloc
    mov r15, rax ; save pointer to allocated space

    mov rdi, mal_point 
    call std_put_str

    mov rdi, r15
    call std_put_int_ln

    mov rdi, mal_w_msg     
    call std_put_str

    mov rdi, buffer
    mov rsi, BUFFER_LEN
    call std_get_line

    mov rdi, r15
    mov rsi, buffer
    call std_strcpy

    mov rdi, r15
    call std_put_str_ln
    
    mov rdi, mal_free 
    call std_put_str

    mov rdi, r15
    call std_put_int_ln

    ;;;;;; Exit ;;;;;
    mov rdi, 0
    call std_exit



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Test malloc
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
test_malloc:
    mov rdi, malloc_msg
    call std_put_str
    
    mov rdi, 256
    call std_malloc 
 
    cmp rax, 0
    je .fail

    push rax ; save rax
    mov rdi, succ_msg
    call std_put_str_ln
    pop rax ;restore rax
    ret

.fail:
    mov rdi, fail_msg
    call std_put_str_ln
    ret


