;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; System V AMD64 ABI:
;; ~~~~~~~~~~~~~~~~~~~
;;
;; The first six integer or pointer arguments are passed in registers RDI, RSI, RDX,
;; RCX, R8, R9 (R10 is used as a static chain pointer in case of nested functions), while
;; XMM0, XMM1, XMM2, XMM3, XMM4, XMM5, XMM6 and XMM7 are used for certain floating point
;; arguments. Additional arguments are passed on the stack. Integral return values
;; up to 64 bits in size are stored in RAX while values up to 128 bit are stored in RAX
;; and RDX. Floating-point return values are similarly stored in XMM0 and XMM1.
;;
;; If the callee wishes to use registers RBX, RBP, and R12–R15, it must restore their
;; original values before returning control to the caller. All other registers must be
;; saved by the caller if it wishes to preserve their values.
;;
;; Link: https://github.com/hjl-tools/x86-psABI/wiki/X86-psABI
;;
;; Arguments   : rdi, rsi, rdx, rcx, r8, r9
;; Callee Saved: rbx, rbp, r12, r13, r14, r15
;;
;; I don't use the stack base pointer here, because this functions never have more than
;; six arguments and therfore i access the stack only for local variables defined at the 
;; beginning.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

%define NEWLINE 10

section .data
    dbg_msg_ok db "OK",0
    dbg_msg_err db "ERR: ",0

section .text
    extern malloc
    extern free
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Print a char. Argument is a pointer to a character.
;; Only ASCII is supported.
;; int std_put_c (uint8_t)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
std_put_c:
    sub rsp, 1              ; local variable for one byte
    mov [rsp], dil          ; 1 byte low register of rdi
    
	mov rax, 1              ; syscall to sys_write
	mov rdi, 1              ; stdout
    mov rsi, rsp            ; pointer to byte on stack
	mov rdx, 1              ; buffer len
	syscall                 

    add rsp, 1              ; destroy local variable
    ret                    

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Print a char with newline. Argument is a pointer to a character.
;; Only ASCII is supported.
;; int std_put_c_ln (uint8_t)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
std_put_c_ln:
    sub rsp, 2              ; local variable for two byte
    mov [rsp], dil
    mov [rsp+1], byte NEWLINE

	mov rax, 1              ; syscall to sys_write
	mov rdi, 1              ; stdout
    mov rsi, rsp            ; pointer to two byte on stack
	mov rdx, 2              ; buffer len
	syscall                 

    add rsp, 2              ; destroy local variable   
    ret                     


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Print a string. Arguent is a pointer to a null terminated string
;; Does somewhat support multi byte characters, but not really.
;; int std_put_str (uint8_t*)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
std_put_str:
    mov rcx, rdi            ; rcx = pointer zu string (erstes argument)

    mov rax, 1              ; syscall to sys_write
	mov rdi, 1              ; stdout
    mov rsi, rcx            ; buffer pointer sys_write
    mov rdx, 0              ; buffer len for sys_write

.not_null:
    movsx r8, byte [rcx]
    inc rcx 
    inc rdx
    cmp r8b, byte 0
    jne .not_null

    dec rdx
	syscall                 ; führe syscall aus
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Print a string with newline on the end. Arguent is a pointer to a null terminated string
;; Does somewhat support multi byte characters, but not really.
;; int std_put_str_ln (uint8_t*)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
std_put_str_ln:
    call std_put_str
    mov rdi, NEWLINE
    call std_put_c
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Return the next argument of argv. All strings must be terminated with null and must be in
;; one continouse memory area
;; Only ASCII is supported.
;; int std_tok_spc (char*)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
std_tok_spc:
    mov rax, rdi            ; rax = pointer zu string (erstes argument)
.not_null:
    movsx rcx, byte [rax]
    inc rax 
    cmp cl, byte 0
    jne .not_null
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Converts a uint to a string and prints it. But please.. don't make the int to big.
;; int std_put_int (int)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
std_put_int:
    sub rsp, 21             ; array of 21 byte, should hold every positive 64 bit number
                            ; in ascii representation, if not bufferoverflow :-)
    mov rcx, rsp            ; pointer to buffer
    mov rdx, 0              ; in rdx:rax ist der nenner wir gehen von max 64 bit nenner aus
    mov rax, rdi            ; wert der konvertiert werden soll
    mov r8, 10              ; divisor
    mov r9, 0               ; counter
    push 0                  ; push null for string stop
    inc r9

.loop:
    cmp rax, 10             ; if the divident is smaller 10 we are finished all but one digit
    jl .loop_fin

    div r8                  ; teile rdx:rax durch 10. Erg in rax, rest in rdx
    add rdx, '0'            ; convert digit to char
    push rdx                ; push char to stack
    inc r9                  ; increment counter. It is used to pop the chars in the str_buf
    mov rdx, 0              ; clear the upper half of the divident
    jmp .loop
    
.loop_fin:
    add rax, '0'            ; convert tha last digit to char
    push rax 
    inc r9

.cpy:
    cmp r9, 0               ; copy the string on the stack to str_buf
    je .cpy_fin
    
    pop rax 
    mov [rcx], al
    inc rcx
    dec r9
    jmp .cpy
    
.cpy_fin:
    mov rdi, rsp            ; print the number in str_buf number
    call std_put_str        ; we don't need rax, rcx or rdx anymore
    add rsp, 21 
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Converts a uint to a string and prints it with newline.
;; But please.. don't make the int to big.
;; int std_put_int_ln (int)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
std_put_int_ln:
    call std_put_int
    mov rdi, NEWLINE
    call std_put_c
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Gets an char from stdin and reads the trailing newline
;; Only ASCII is supported.
;; int std_get_c (void)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
std_get_c:
    sub rsp, 2              ; buffer for char and newline

    mov rax, 0              ; syscall to sys_read
    mov rdi, 0              ; stdin for sys_read
    mov rsi, rsp            ; buffer pointer sys_read
    mov rdx, 2              ; we want one char and the newline from sys_read
    syscall
    cmp rax, 2
    jne .error
    mov al, byte [rsp]
.error:
    add rsp, 2              ; destroy buffer
    ret
 
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Does somewhat support multi byte characters, but not really.
;; void std_get_line uint8_t* buf, size_t len
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
std_get_line:
    mov r8, rdi             ; Buffer 
    mov r9, r8            
    add r9, rsi             ; buffer len, first address not in buffer
.loop:
    mov rax, 0              ; syscall to sys_read
    mov rdi, 0              ; stdin for sys_read
    mov rsi, r8             ; buffer pointer sys_read
    mov rdx, 1              ; we want one char from sys_read
    syscall

    inc r8 

    cmp r8, r9              ; check if buffer is full
    je .fin

    cmp rax, 1              ; chek if a error occured
    jne .fin

    cmp byte [r8-1], NEWLINE       ; check if we read a newline
    je .fin

    jmp .loop

.fin:
    mov [r8-1], byte 0      ; If the buffer is full r8-1 is the last byte in the buffer and
                            ; we write a 0 to it.
                            ; If we found a newline r8-1 is the newline and it is replaced
                            ; by 0. If we detect an error the last valid char is replaced by
                            ; 0. But that is not that bad. It is more important to have a 
                            ; valid string in case of an error.
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; strcpy(char* dest, char* src)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
std_strcpy:
    mov dl, byte [rsi]
    mov [rdi], dl

    inc rdi
    inc rsi

    cmp dl, 0
    jne std_strcpy 
    ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Exit a programm with status code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
std_exit:
	mov rax, 60             ; syscall to exit
                            ; rdi is already filled with the exit value
	syscall                 ; call exit


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Just malloc and free from standard library
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
std_malloc:
    call malloc
    ret

std_free:
    call free
    ret
