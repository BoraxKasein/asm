; https://0xax.github.io/asm_1/

; .data section is for constants
section .data
	msg db 'Hello world',0x0A
	len equ $-msg

; .text section is for code
; must start with global _start
; kernel starts the program at _start
section .text
	global _start

; rax enthält syscall nummer für sys_write (1)
; size_t sys_write(unsigned int fd, const char * buf, size_t count);
; rdi ist für das erste argument, hier stdout (1)
; rsi ist für das zweite argument, hier msg
; rdx ist für das dritte argument, hier die länge von msg also "Hello World"
_start:
	mov rax, 1      ; syscall to sys_write
	mov rdi, 1      ; stdout
	mov rsi, msg    ; buffer
	mov rdx, len    ; buffer len
	syscall         ; führe syscall aus
	mov rax, 60     ; syscall to exit
	mov rdi, 0      ; erster parameter 0
	syscall         ; call exit
